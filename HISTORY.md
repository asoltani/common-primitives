## vNEXT
* Added `RemoveColumnsPrimitive` which removes columns from a Dataframe resource in a Dataset
* Added `UpdateSemanticTypesPrimitive` which modifies semantic type information for a resource in a Dataset
* Added `DatasetToDataFramePrimitive` which extracts a DataFrame resource from
  a Dataset.
* Combined `ExtractTargetsPrimitive` and `ExtractAttributesPrimitive` into
  `ExtractColumnsBySemanticTypesPrimitive` primitive.
* Created `devel` branch which contains primitives coded against the
  future release of the `d3m` core package (its `devel` branch).
  `master` branch of this repository is made against the latest stable
  release of the `d3m` core package.
* Dropped support for Python 2.7 and require Python 3.6.
* Renamed repository and package to `common-primitives` and `common_primitives`,
  respectively.
* Repository migrated to gitlab.com and made public.

## v0.1.1

* Made common primitives work on Python 2.7.

## v0.1.0

* Initial set of common primitives.
