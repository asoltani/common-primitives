import copy
import os

from d3m import container, utils as d3m_utils
from d3m.metadata import base as metadata_base, hyperparams
from d3m.primitive_interfaces import base, transformer
from common_primitives import utils
import common_primitives

__all__ = ('UpdateSemanticTypesPrimitive',)

Inputs = container.Dataset
Outputs = container.Dataset


class Hyperparams(hyperparams.Hyperparams):
    resource_id = hyperparams.Hyperparameter[str](
        default='0',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='Resource ID of columns to remove'
    )
    add_columns = hyperparams.Set(
        hyperparams.Hyperparameter[str](''),
        default=(),
        description='Columns to add to, corresponding types are in \'add_types\'',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter']
    )
    add_types = hyperparams.Set(
        hyperparams.Hyperparameter[str](''),
        default=(),
        description='Types to add, target columns are defined in \'add_columns\'',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter']
    )
    remove_columns = hyperparams.Set(
        hyperparams.Hyperparameter[str](''),
        default=(),
        description='Columns to remove from, corresponding types are in \'remove_types\'',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter']
    )
    remove_types = hyperparams.Set(
        hyperparams.Hyperparameter[str](''),
        default=(),
        description='Types to remove, target columns are defined in \'remove_columns\'',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter']
    )


class UpdateSemanticTypesPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which adds and remove the semantic type info for a container's columns.  Hyperparameter
    definition is awkward due to the structure of the TA3/TA2 API, and will be addressed in a future release.
    """
    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': '98c79128-555a-4a6b-85fb-d4f4064c94ab',
            'version': '0.1.1',
            'name': "Semantic type updater",
            'python_path': 'd3m.primitives.datasets.UpdateSemanticTypes',
            'source': {
               'name': common_primitives.__author__,
            },
            'installation': [{
               'type': metadata_base.PrimitiveInstallationType.PIP,
               'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                   git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
               ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.DATA_CONVERSION,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION,
        },
    )

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        resource_id = self.hyperparams['resource_id']

        # create a column name -> index LUT
        num_cols = inputs.metadata.query((resource_id, metadata_base.ALL_ELEMENTS,))['dimension']['length']
        name_idx_map = {}
        for i in range(0, num_cols):
            name_idx_map[(inputs.metadata.query((resource_id, metadata_base.ALL_ELEMENTS, i))['name'])] = i

        new_metadata = inputs.metadata

        remove_columns = self.hyperparams['remove_columns']
        remove_types = self.hyperparams['remove_types']
        for i in range(0, len(remove_columns)):
            col_name = remove_columns[i]
            semantic_type = remove_types[i]

            # remove value from semantic type list
            selector = (resource_id, metadata_base.ALL_ELEMENTS, name_idx_map[col_name])
            col_metadata = dict(new_metadata.query(selector))
            removed_types = [t for t in col_metadata['semantic_types'] if t != semantic_type]
            col_metadata['semantic_types'] = removed_types

            # replace the semantic type list in the dataframe.
            new_metadata = new_metadata.update(selector, col_metadata)

        add_columns = self.hyperparams['add_columns']
        add_types = self.hyperparams['add_types']
        for i in range(0, len(add_columns)):
            col_name = add_columns[i]
            semantic_type = add_types[i]

            # add values to semantic type list
            selector = (resource_id, metadata_base.ALL_ELEMENTS, name_idx_map[col_name])
            col_metadata = dict(new_metadata.query(selector))
            added_types = list(col_metadata['semantic_types'])
            added_types.append(semantic_type)
            col_metadata['semantic_types'] = added_types

            # replace the semantic type list in the dataframe.
            new_metadata = new_metadata.update(selector, col_metadata)

        outputs = copy.copy(inputs)
        new_metadata.set_for_value(outputs, generate_metadata=False)
        outputs.metadata = new_metadata

        return base.CallResult(outputs)
