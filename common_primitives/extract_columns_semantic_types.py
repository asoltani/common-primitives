import os
import typing

from d3m import container, utils as d3m_utils
from d3m.metadata import base as metadata_base, hyperparams
from d3m.primitive_interfaces import base, transformer

import common_primitives
from common_primitives import utils

__all__ = ('ExtractColumnsBySemanticTypesPrimitive',)

Inputs = container.DataFrame
Outputs = container.DataFrame


class Hyperparams(hyperparams.Hyperparams):
    semantic_types = hyperparams.Set(
        elements=hyperparams.Hyperparameter[str](''),
        default=('https://metadata.datadrivendiscovery.org/types/Attribute',),
        min_size=1,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Semantic types to use to extract columns. If any of them matches.",
    )
    use_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to force primitive to operate on. If any specified column does not match any semantic type, it is skipped.",
    )
    exclude_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to not operate on. Applicable only if \"use_columns\" is not provided.",
    )


class ExtractColumnsBySemanticTypesPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which extracts columns from input data based on semantic types provided.
    Columns which match any of the listed semantic types are extracted.

    If you want to extract only attributes, you can use ``https://metadata.datadrivendiscovery.org/types/Attribute``
    semantic type (also default).

    For real targets (not suggested targets) use ``https://metadata.datadrivendiscovery.org/types/Target``.
    For this to work, columns have to be are marked as targets by the TA2 in a dataset before passing the dataset
    through a pipeline. Or something else has to mark them at some point in a pipeline.

    It uses ``use_columns`` and ``exclude_columns`` to control which columns it considers.
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': '4503a4c6-42f7-45a1-a1d4-ed69699cf5e1',
            'version': '0.2.0',
            'name': "Extracts columns by semantic type",
            'python_path': 'd3m.primitives.data.ExtractColumnsBySemanticTypes',
            'source': {
               'name': common_primitives.__author__,
            },
            'installation': [{
               'type': metadata_base.PrimitiveInstallationType.PIP,
               'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                   git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
               ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.ARRAY_SLICING,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION,
        },
    )

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        outputs = self._produce(inputs, self.hyperparams, self)

        return base.CallResult(outputs)

    @classmethod
    def _produce(cls, inputs: Inputs, hyperparams: Hyperparams, source: typing.Any) -> Outputs:
        columns = cls._get_columns(inputs.metadata, hyperparams)

        outputs = inputs.iloc[:, columns]
        outputs.metadata = utils.metadata_select_columns(inputs.metadata, columns, source=source)
        outputs.metadata = outputs.metadata.set_for_value(outputs, source=source)

        return outputs

    @classmethod
    def _get_columns(cls, inputs_metadata: metadata_base.DataMetadata, hyperparams: hyperparams.Hyperparams) -> typing.Sequence[int]:
        columns = utils.metadata_list_columns_with_semantic_types(inputs_metadata, hyperparams['semantic_types'])

        def can_produce_column(column_index: int) -> bool:
            return column_index in columns

        columns_to_produce, columns_not_to_produce = utils.get_columns_to_produce(inputs_metadata, hyperparams, can_produce_column)

        if not columns_to_produce:
            raise ValueError("Input data has no columns matching semantic types: {semantic_types}".format(
                semantic_types=hyperparams['semantic_types'],
            ))

        if hyperparams['use_columns'] and columns_not_to_produce:
            cls.logger.warning("Not all specified columns match semantic types. Skipping columns: %(columns)s", {
                'columns': columns_not_to_produce,
            })

        return columns_to_produce

    @classmethod
    def can_accept(cls, *, method_name: str, arguments: typing.Dict[str, typing.Union[metadata_base.Metadata, type]],
                   hyperparams: Hyperparams) -> typing.Optional[metadata_base.DataMetadata]:
        output_metadata = super().can_accept(method_name=method_name, arguments=arguments, hyperparams=hyperparams)

        # If structural types didn't match, don't bother.
        if output_metadata is None:
            return None

        if 'inputs' not in arguments:
            return output_metadata

        inputs_metadata = typing.cast(metadata_base.DataMetadata, arguments['inputs'])

        columns = cls._get_columns(inputs_metadata, hyperparams)

        return utils.metadata_select_columns(inputs_metadata, columns, source=cls)
