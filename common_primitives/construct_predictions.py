import collections
import copy
import os
import typing

from d3m import container, utils as d3m_utils
from d3m.metadata import base as metadata_base, hyperparams
from d3m.primitive_interfaces import base, transformer

import common_primitives
from common_primitives import extract_columns_semantic_types, horizontal_concat, utils

__all__ = ('ConstructPredictionsPrimitive',)

Inputs = container.DataFrame
Targets = container.DataFrame
Outputs = container.DataFrame


class Hyperparams(hyperparams.Hyperparams):
    pass


class ConstructPredictionsPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which takes as input a DataFrame with results from a wrapped sklearn primitive
    and outputs a DataFrame suitable for pipeline output.
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': '8d38b340-f83f-4877-baaa-162f8e551736',
            'version': '0.2.0',
            'name': "Construct pipeline predictions output",
            'python_path': 'd3m.primitives.data.ConstructPredictions',
            'source': {
               'name': common_primitives.__author__,
            },
            'installation': [{
               'type': metadata_base.PrimitiveInstallationType.PIP,
               'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                   git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
               ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.DATA_CONVERSION,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION,
        },
    )

    def produce(self, *, inputs: Inputs, targets: Targets, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:  # type: ignore
        """
        When producing, ``inputs`` is a ``DataFrame`` before attributes were extracted.
        It should contain all samples which are then passed to a wrapped sklearn primitive.
        It is used to get ``d3mIndex`` column.

        ``targets`` is the output from the wrapper sklearn primitive.
        """

        index_column = self._extract_columns_by_semantic_types(inputs, ('https://metadata.datadrivendiscovery.org/types/PrimaryKey',))

        if targets.metadata.get_columns_with_semantic_type('https://metadata.datadrivendiscovery.org/types/PredictedTarget'):
            targets = self._extract_columns_by_semantic_types(targets, ('https://metadata.datadrivendiscovery.org/types/PrimaryKey', 'https://metadata.datadrivendiscovery.org/types/PredictedTarget',))
        else:
            targets = copy.copy(targets)
            # We patch things up.
            targets.metadata = self._add_semantic_types(targets.metadata, self)

        targets.metadata = self._set_target_names(targets.metadata, self._get_target_names(inputs.metadata), self)

        outputs = self._horizontal_concat(index_column, targets)

        return base.CallResult(outputs)

    def multi_produce(self, *, produce_methods: typing.Sequence[str], inputs: Inputs, targets: Targets, timeout: float = None, iterations: int = None) -> base.MultiCallResult:  # type: ignore
        return self._multi_produce(produce_methods=produce_methods, timeout=timeout, iterations=iterations, inputs=inputs, targets=targets)

    def _extract_columns_by_semantic_types(self, inputs: container.DataFrame, semantic_types: typing.Sequence[str]) -> container.DataFrame:
        hyperparams_class = extract_columns_semantic_types.ExtractColumnsBySemanticTypesPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        return extract_columns_semantic_types.ExtractColumnsBySemanticTypesPrimitive._produce(
            inputs,
            hyperparams_class.defaults().replace({'semantic_types': semantic_types}),
            source=self,
        )

    def _horizontal_concat(self, left: container.DataFrame, right: container.DataFrame) -> container.DataFrame:
        hyperparams_class = horizontal_concat.HorizontalConcatPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        return horizontal_concat.HorizontalConcatPrimitive._produce(
            left,
            right,
            hyperparams=hyperparams_class.defaults(),
            source=self,
        )

    @classmethod
    def _get_target_names(cls, metadata: metadata_base.DataMetadata) -> typing.List[str]:
        target_names = []

        for column_index in metadata.get_columns_with_semantic_type('https://metadata.datadrivendiscovery.org/types/Target'):
            if column_index is metadata_base.ALL_ELEMENTS:
                continue

            column_index = typing.cast(metadata_base.SimpleSelectorSegment, column_index)

            column_metadata = metadata.query((metadata_base.ALL_ELEMENTS, column_index))

            target_names.append(column_metadata.get('name', str(column_index)))

        return target_names

    @classmethod
    def _set_target_names(cls, metadata: metadata_base.DataMetadata, target_names: typing.Sequence[str], source: typing.Any) -> metadata_base.DataMetadata:
        targets_length = metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']

        primary_indices_length = len(metadata.get_columns_with_semantic_type('https://metadata.datadrivendiscovery.org/types/PrimaryKey'))

        if len(target_names) != targets_length - primary_indices_length:
            raise ValueError("Not an expected number of target columns provided. Expected {target_names}, provided {targets_length}.".format(
                target_names=len(target_names),
                targets_length=targets_length - primary_indices_length,
            ))

        for column_index, target_name in enumerate(target_names):
            column_metadata = metadata.query_column(column_index + primary_indices_length)

            if 'name' not in column_metadata:
                metadata = metadata.update_column(column_index + primary_indices_length, {
                    'name': target_name,
                }, source=source)
            elif target_name != column_metadata['name']:
                cls.logger.warning("Target name mismatch at target with index {column_index}. Expected '{expected}', provided '{provided}'.".format(
                    column_index=column_index + primary_indices_length,
                    expected=target_name,
                    provided=column_metadata['name'],
                ))

        return metadata

    @classmethod
    def _add_semantic_types(cls, metadata: metadata_base.DataMetadata, source: typing.Any) -> metadata_base.DataMetadata:
        for column_index in range(metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']):
            metadata = metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, column_index), 'https://metadata.datadrivendiscovery.org/types/Target', source=source)
            metadata = metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, column_index), 'https://metadata.datadrivendiscovery.org/types/PredictedTarget', source=source)

        return metadata

    @classmethod
    def can_accept(cls, *, method_name: str, arguments: typing.Dict[str, typing.Union[metadata_base.Metadata, type]], hyperparams: Hyperparams) -> typing.Optional[metadata_base.DataMetadata]:
        output_metadata = super().can_accept(method_name=method_name, arguments=arguments, hyperparams=hyperparams)

        # If structural types didn't match, don't bother.
        if output_metadata is None:
            return None

        if 'inputs' not in arguments or 'targets' not in arguments:
            return output_metadata

        inputs_metadata = typing.cast(metadata_base.DataMetadata, arguments['inputs'])
        targets_metadata = typing.cast(metadata_base.DataMetadata, arguments['targets'])

        index_column = cls._get_index_column(inputs_metadata)
        target_names = cls._get_target_names(inputs_metadata)

        if len(target_names) != targets_metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']:
            raise ValueError("Not all expected target columns provided.")

        outputs_metadata = cls._select_columns(inputs_metadata, [index_column], source=cls)

        outputs_metadata = cls._add_semantic_types(outputs_metadata, targets_metadata, target_names, cls)

        return outputs_metadata
