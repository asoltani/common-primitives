import os
import typing

import pandas  # type: ignore

from d3m import container, utils as d3m_utils
from d3m.metadata import base as metadata_base, hyperparams
from d3m.primitive_interfaces import base, transformer

import common_primitives
from common_primitives import utils

__all__ = ('HorizontalConcatPrimitive',)

Inputs = container.DataFrame
Outputs = container.DataFrame


class Hyperparams(hyperparams.Hyperparams):
    use_index = hyperparams.UniformBool(
        default=True,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Use primary index columns in both DataFrames (if they exist) to match rows in proper order. Otherwise, concatination happens on the order of rows in input DataFrames.",
    )
    remove_second_index = hyperparams.UniformBool(
        default=True,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="When both input DataFrames have a primary index column, remove the second index column from the result."
        " When \"use_index\" is \"True\", the second index column is redundant because it is equal to the first one (assuming equal metadata).",
    )


class HorizontalConcatPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which concatenates two DataFrames horizontally.

    It is required that both DataFrames have the same number of samples.
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': 'aff6a77a-faa0-41c5-9595-de2e7f7c4760',
            'version': '0.2.0',
            'name': "Concatenate two dataframes",
            'python_path': 'd3m.primitives.data.HorizontalConcat',
            'source': {
               'name': common_primitives.__author__,
            },
            'installation': [{
               'type': metadata_base.PrimitiveInstallationType.PIP,
               'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                   git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
               ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.ARRAY_CONCATENATION,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION,
        },
    )

    def produce(self, *, left: Inputs, right: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:  # type: ignore
        outputs = self._produce(left, right, self.hyperparams, self)

        return base.CallResult(outputs)

    @classmethod
    def _produce(cls, left: Inputs, right: Inputs, hyperparams: Hyperparams, source: typing.Any) -> Outputs:
        cls._check_dimensions(left.metadata, right.metadata)

        left_index = cls._get_index_column(left.metadata)
        right_index = cls._get_index_column(right.metadata)

        if left_index is not None and right_index is not None:
            if hyperparams['use_index']:
                # TODO: Reorder metadata rows as well.
                #       This should be relatively easy because we can just modify
                #       right.metadata._current_metadata.metadata map (and create a new action type for the log).
                right = right.set_index(right.iloc[:, right_index]).reindex(left.iloc[:, left_index]).reset_index(drop=True)

            # Removing second primary key column.
            if hyperparams['remove_second_index']:
                right = utils.remove_column(right, right_index, source)

        outputs = pandas.concat([left, right], axis=1)
        outputs.metadata = left.metadata.set_for_value(outputs, generate_metadata=False, source=source)
        outputs.metadata = utils.append_metadata(outputs.metadata, right.metadata, source=self)
        outputs.metadata = outputs.metadata.update((metadata_base.ALL_ELEMENTS,), {'dimension': {'length': outputs.shape[1]}}, source=self)

        return outputs

    @classmethod
    def _produce_metadata(cls, left_metadata: metadata_base.DataMetadata, right_metadata: metadata_base.DataMetadata, hyperparams: Hyperparams) -> metadata_base.DataMetadata:
        cls._check_dimensions(left_metadata, right_metadata)

        left_index = cls._get_index_column(left_metadata)
        right_index = cls._get_index_column(right_metadata)

        if left_index is not None and right_index is not None:
            if hyperparams['use_index']:
                # TODO: Reorder metadata rows as well.
                pass

            # Removing second primary key column.
            if hyperparams['remove_second_index']:
                right_metadata = utils.remove_column_metadata(right_metadata, right_index, cls)

        left_length = left_metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']
        right_length = right_metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']

        outputs_metadata = utils.append_metadata(left_metadata, right_metadata, source=cls)
        outputs_metadata = outputs_metadata.update((metadata_base.ALL_ELEMENTS,), {'dimension': {'length': left_length + right_length}}, source=cls)

        return outputs_metadata

    def multi_produce(self, *, produce_methods: typing.Sequence[str], left: Inputs, right: Inputs, timeout: float = None, iterations: int = None) -> base.MultiCallResult:  # type: ignore
        return self._multi_produce(produce_methods=produce_methods, timeout=timeout, iterations=iterations, left=left, right=right)

    @classmethod
    def _check_dimensions(cls, left_metadata: metadata_base.DataMetadata, right_metadata: metadata_base.DataMetadata) -> None:
        if left_metadata.query(())['dimension']['length'] != right_metadata.query(())['dimension']['length']:
            raise ValueError("Left and right inputs do not have the same number of samples.")

    @classmethod
    def _get_index_column(cls, metadata: metadata_base.DataMetadata) -> typing.Optional[int]:
        index_columns = metadata.get_columns_with_semantic_type('https://metadata.datadrivendiscovery.org/types/PrimaryKey')
        assert len(index_columns) < 2
        if index_columns:
            index = typing.cast(int, index_columns[0])
        else:
            index = None

        return index

    @classmethod
    def can_accept(cls, *, method_name: str, arguments: typing.Dict[str, typing.Union[metadata_base.Metadata, type]], hyperparams: Hyperparams) -> typing.Optional[metadata_base.DataMetadata]:
        output_metadata = super().can_accept(method_name=method_name, arguments=arguments, hyperparams=hyperparams)

        # If structural types didn't match, don't bother.
        if output_metadata is None:
            return None

        if 'left' not in arguments and 'right' not in arguments:
            return output_metadata

        left_metadata = typing.cast(metadata_base.DataMetadata, arguments['left'])
        right_metadata = typing.cast(metadata_base.DataMetadata, arguments['right'])

        return cls._produce_metadata(left_metadata, right_metadata, hyperparams)
