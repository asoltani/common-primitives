import os
import sys
import copy

from d3m import container, utils as d3m_utils
from d3m.metadata import base as metadata_base, hyperparams
from d3m.primitive_interfaces import base, transformer
from common_primitives import utils
import common_primitives

__all__ = ('RemoveColumnsPrimitive',)

Inputs = container.Dataset
Outputs = container.Dataset


class Hyperparams(hyperparams.Hyperparams):
    resource_id = hyperparams.Hyperparameter[str](
        default='0',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='resource ID of columns to remove'
    )
    columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[str](''),
        default=(),
        max_size=sys.maxsize,
        min_size=0,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='names of columns to remove'
    )


class RemoveColumnsPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which removes columns from a dataframe.  Columns are specified by name, comparison is case sensitive
    for consistency with underlying dataframe drop behavior.
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': '2eeff053-395a-497d-88db-7374c27812e6',
            'version': '0.1.1',
            'name': "Column remover",
            'python_path': 'd3m.primitives.datasets.RemoveColumns',
            'source': {
               'name': common_primitives.__author__,
            },
            'installation': [{
               'type': metadata_base.PrimitiveInstallationType.PIP,
               'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                   git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
               ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.ARRAY_SLICING,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION,
        },
    )

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        resource_id = self.hyperparams['resource_id']
        resource = inputs[resource_id]

        num_cols = inputs.metadata.query((resource_id, metadata_base.ALL_ELEMENTS))['dimension']['length']

        # compute the set of columns to keep
        param_column_names = self.hyperparams['columns']
        column_names = set([(inputs.metadata.query((resource_id, metadata_base.ALL_ELEMENTS, i))['name']) for i in range(0, num_cols)])
        keep_columns = column_names.difference(param_column_names)

        # update the column count
        new_metadata = inputs.metadata.update(
            (resource_id, metadata_base.ALL_ELEMENTS,),
            {
                'dimension': {
                    'length': len(keep_columns),
                },
            },
            source=self,
        )

        # shift the keep columns to the left
        new_col_num = 0
        for i in range(0, num_cols):
            source_col_metadata = inputs.metadata.query((resource_id, metadata_base.ALL_ELEMENTS, i))
            if source_col_metadata['name'] in keep_columns:
                new_metadata = new_metadata.update((resource_id, metadata_base.ALL_ELEMENTS, new_col_num), source_col_metadata)
                new_col_num += 1

        # trim the remainder
        for i in range(len(keep_columns), num_cols):
            new_metadata = new_metadata.remove_column(i, at=(resource_id,), source=self)

        outputs = copy.copy(inputs)
        outputs[resource_id] = resource.drop(columns=list(param_column_names))
        new_metadata.set_for_value(outputs, generate_metadata=False)
        outputs.metadata = new_metadata

        return base.CallResult(outputs)
