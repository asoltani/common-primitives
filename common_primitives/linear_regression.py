import time
import os
import math
import random

from typing import NamedTuple, Sequence, Any, List, Dict, Union, Tuple
from d3m.container.list import List
from d3m.container.numpy import ndarray
import torch  # type: ignore
import numpy as np  # type: ignore
from common_primitives.diagonal_mvn import DiagonalMVN, Hyperparams as MVNHyperparams
from sklearn.metrics import mean_squared_error # type: ignore

from d3m.metadata import hyperparams, params
import d3m.metadata.base as metadata_module
from d3m import utils
from d3m.primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
from d3m.primitive_interfaces.base import (
    ProbabilisticCompositionalityMixin,
    GradientCompositionalityMixin,
    SamplingCompositionalityMixin,
    CallResult,
    Gradients,
    DockerContainer,
)

# TODO just only use the map estimate everywhere instead of sampling a weight from the posterior of the weights where it is currently


from .utils import to_variable, refresh_node, log_mvn_likelihood

# this is a bit confusing here. the linreg primitive is normal(Xw, I)
# so we want the inputs here to be the actual inputs as well as the weights
# this is because in end to end trianing, we will need to produce dE/dw and dE/dx
# in practice, the weights are not given as inputs per se but specified here because
# we need it in backward gradient params.
Inputs = ndarray
# 1D array of floats of length n_inputs
Outputs = ndarray

# A named tuple for parameters.
# Specifying types for all parameters is important so that one can do end-to-end
# training from outside. For example, some parameters might have gradients so we
# can use those to optimize them end-to-end.
class Hyperparams(hyperparams.Hyperparams):
    weights_prior = hyperparams.Hyperparameter[Union[None, GradientCompositionalityMixin]](
                            semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
                            default=None,
                            description='prior on weights'
                            )
    learning_rate = hyperparams.Hyperparameter[float](
                            semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
                            default=1e-3,
                            description='a tuning parameter for analytic fit'
                            )
    batch_size = hyperparams.Hyperparameter[float](
                            semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
                            default=1,
                            description='a tuning parameter for analytic fit'
                            )
    tune_prior_end_to_end = hyperparams.Hyperparameter[int](
                            semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
                            default=False,
                            description='setting this to true will case the end to end training to propogate \
                                        back to the prior parameters. what this means is that the actual \
                                        parameters of the prior distribution are going to be changed\
                                        according to the chain rule.'
                            )
    analytic_fit_threshold = hyperparams.Hyperparameter[int](
                            semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
                            default=100,
                            description='the threshold under which analytic fit is still used. \
                                        Realistically, over 1000 cannot be done, but this hyperparam can also \
                                        be used to force gradient fitting even if analytic solution is possible \
                                        i.e. by setting to 0'
                            )


class Params(params.Params):
    weights: ndarray
    weights_variance: ndarray
    offset: float
    noise_variance: float

class LinearRegression(ProbabilisticCompositionalityMixin[Inputs, Outputs, Params, Hyperparams],
                       GradientCompositionalityMixin[Inputs, Outputs, Params, Hyperparams],
                       SamplingCompositionalityMixin[Inputs, Outputs, Params, Hyperparams],
                       # Base class should be after all mixins.
                       SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):

    """
    Example of a primitive wrapping implementing a linear regression using a high-performance
    PyTorch backend, providing methods from multiple mixins.
    """

    __author__ = "Oxford DARPA D3M Team, Michael Teng <mteng@robots.ox.ac.uk>"
    metadata = metadata_module.PrimitiveMetadata({
         'id': '28ad180e-ef88-427a-ac00-0673efc72b3b',
         'version': '0.1.0',
         'name': 'Bayesian linear regression',
         'keywords': ['Bayesian', 'regression'],
         'source': {
            'name': 'common-primitives',
            'contact' : 'mailto:mteng@robots.ox.ac.uk'
         },
         'installation': [{
            'type': metadata_module.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common-primitives'.format(git_commit=utils.current_git_commit(os.path.dirname(__file__)))
         }],
         'python_path': 'd3m.primitives.common_primitives.LinearRegression',
         'algorithm_types': ['LINEAR_REGRESSION'],
         'primitive_family': 'OPERATOR',
    })

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, DockerContainer] = None) -> None:
        super().__init__(hyperparams=hyperparams, random_seed=random_seed, docker_containers=docker_containers)

        self._learning_rate = hyperparams['learning_rate']
        self._analytic_fit_threshold = hyperparams['analytic_fit_threshold']
        self._batch_size = hyperparams['batch_size']
        self._analytic_fit_threshold = hyperparams['analytic_fit_threshold']
        self._weights_prior = hyperparams['weights_prior']
        self._tune_prior_end_to_end = hyperparams['tune_prior_end_to_end']

        self._fit_term_temperature = 0.0

        self._weights = None  # type: torch.autograd.Variable

        # the noise variance is what we are calling the variance of the noise model
        # i.e. I in the original likelihood N(Xw, I)
        self._noise_variance = None
        self._weights_variance = None

        self._training_inputs = None  # type: torch.autograd.Variable
        self._training_outputs = None  # type: torch.autograd.Variable
        self._iterations_done = None  # type: int
        self._has_finished = False
        self._new_training_data = True
        self._inputs = None
        self._outputs = None
        self._use_analytic_form = False

    def backward(self, *, gradient_outputs: Gradients[Outputs], fine_tune: bool = False, fine_tune_learning_rate: float = 0.00001) -> Tuple[Gradients[Inputs], Gradients[Params]]:  # type: ignore
        if self._inputs is None \
                or self._weights is None \
                or self._noise_variance is None \
                or self._weights_variance is None \
                or self._outputs is None:
            raise Exception('Cannot call backpropagation before forward propagation. Call "produce" before "backprop".')
        else:
            if self._inputs.grad is not None: # type: ignore
                self._inputs.grad.data.zero_() # type: ignore

            self._outputs.backward(gradient=torch.Tensor(gradient_outputs)) # type: ignore

            # this is the gradients given by end to end loss
            weights_grad = self._weights.grad.data # type:ignore
            noise_grad = self._noise_variance.grad.data # type:ignore

            if fine_tune:
                # this is gradients given by the annealed local loss
                if self._fit_term_temperature != 0:
                    # TODO use minibatches here
                    training_grads = [self._gradient_params_log_likelihood(output=output, input=input)
                                      for (input, output) in zip(self._training_inputs, self._training_outputs)]
                    weights_grad += self._fit_term_temperature * \
                        sum(grad[0] for grad in training_grads)
                    noise_grad += self._fit_term_temperature * \
                        sum(grad[1] for grad in training_grads)

                # make local update with temperature if required
                # TODO add the score frmo the prior primitive here
                self._weights.data += weights_grad * 1 / torch.norm(weights_grad)
                self._noise_variance.data += noise_grad * 1 / torch.norm(noise_grad) # type: ignore

                self._weights = refresh_node(self._weights)
                self._noise_variance = refresh_node(self._noise_variance)

            grad_inputs = self._inputs.grad # type: ignore
            grad_params = Params(weights=ndarray(weights_grad[:-1]),
                                offset=float(weights_grad[-1]),
                                noise_variance=float(noise_grad[0]),
                                weights_variance=ndarray(np.zeros(self._weights_variance.shape)))   # type: ignore

            if self._tune_prior_end_to_end:
                # update priors parameters here if specified
                self._weights_prior.backward(gradient_outputs=grad['weights'], fine_tune=True) # type: ignore

            return grad_inputs, grad_params # type: ignore


    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        N, P = inputs.shape

        # data matrix is rank deficient
        if N**0.5 < self._analytic_fit_threshold or P < self._analytic_fit_threshold:
            self._use_analytic_form = True

        if P < N and self._weights_prior is None and self._use_analytic_form == True:
            # p < n without regularization while analytically fitting is bad -> add a default mvn over weights
            # TODO
            pass

        inputs_with_ones = np.insert(inputs, P, 1, axis=1)

        self._training_inputs = to_variable(inputs_with_ones, requires_grad=True)
        self._training_outputs = to_variable(outputs, requires_grad=True)
        self._new_training_data = True
        self._has_finished = False
        self._iterations_done = 0
        self._converged_count = 0
        self._best_rmse = np.inf


    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        """
        inputs : (num_inputs,  D) numpy array
        outputs : numpy array of dimension (num_inputs)
        """
        if self._weights is None \
                or self._noise_variance is None \
                or self._weights_variance is None:
            raise Exception('Call fit or set params before calling produce')

        inputs = self._offset_input(inputs=inputs)

        self._weights = refresh_node(self._weights)
        self._noise_variance = refresh_node(self._noise_variance)
        self._weights_variance = refresh_node(self._weights_variance)

        self._inputs = to_variable(inputs, requires_grad=True)
        mu = torch.mm(self._inputs, self._weights.unsqueeze(0).transpose(0, 1)).squeeze()

        reparameterized_normal = torch.distributions.Normal(mu, self._noise_variance.expand(len(mu))) # type: ignore
        # TODO put back reparam trick after 0.4.0
        #  self._outputs = to_variable(reparameterized_normal.rsample())
        self._outputs = to_variable(reparameterized_normal.sample())
        self._outputs.reqiures_grad = True # type: ignore

        return CallResult(self._outputs.data.numpy(), has_finished = self._has_finished, iterations_done=self._iterations_done) # type: ignore


    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        """
        Runs gradient descent for ``timeout`` seconds or ``iterations``
        iterations, whichever comes sooner, on log normal_density(self.weights * self.input
        - output, identity*self.noise_variance) +
        parameter_prior_primitives["weights"].score(self.weights) +
        parameter_prior_primitives["noise_variance"].score(noise_variance).
        """

        if self._new_training_data:
            self._weights = to_variable(torch.FloatTensor(np.random.randn(self._training_inputs.size()[1]) * 0.001))
            self._noise_variance = to_variable(torch.ones(1))
            # this should be a matrix
            self._weights_variance = to_variable(torch.ones(1))
            self._new_training_data = False
        elif self._has_finished:
            # nothing else to do
            return CallResult(None, has_finished=self._has_finished, iterations_done=self._iterations_done)

        if self._use_analytic_form:
            self._analytic_fit()
        else:
            self._gradient_fit(timeout=timeout, iterations=iterations, batch_size=self._batch_size)
        return CallResult(None, has_finished=self._has_finished, iterations_done=self._iterations_done)

    def _analytic_fit(self) -> None:
        train_x = self._training_inputs.data.numpy()
        train_y = self._training_outputs.data.numpy()

        cov_dim = self._training_inputs.shape[1]
        inv_covar = np.zeros([cov_dim, cov_dim])

        if self._weights_prior is not None:
            # just the prior on weights minus offset
            inv_covar[:cov_dim - 1, :cov_dim - 1] = np.linalg.inv(self._weights_prior.get_params()['covariance'])


        # this expression is (X^T*X + Lambda*I)^-1*X^T*Y
        # i.e. it is the solution to the problem argmin_w(E_D(w) + E_w(w))
        # where the first E_D(w) is the ML objective (least squares mvn)
        # and the second term E_w(w) is the regularizer, in this case Lambda/2*w^T*w

        # TODO keep doubling the inv_covar until this is full rank and invertible
        # check first that its invertible
        w_sigma = np.dot(np.transpose(train_x), train_x) + inv_covar * float(self._noise_variance.data.numpy()[0]) # type: ignore
        w_mu = np.dot(np.dot(np.linalg.inv(w_sigma), np.transpose(train_x)), train_y)

        self._weights = to_variable(torch.FloatTensor(w_mu.flatten()))
        self._weights_variance = to_variable(torch.FloatTensor(w_sigma))

        self._iterations_done = 1
        self._has_finished = True

    def _gradient_fit(self, *, timeout: float = None, iterations: int = 100, fit_threshold: float = 0, batch_size: int) -> None:
        if self._training_inputs is None or self._training_outputs is None:
            raise ValueError("Missing training data.")

        if timeout is None:
            # TODO implement this
            timeout = np.inf

        if batch_size is None:
            # TODO come up with something more reasonable here
            batch_size = 1
        x_batches = []
        y_batches = []
        # otpionally do sampling with replacement
        for i in range(0, len(self._training_inputs), batch_size):
            x_batches.append(self._training_inputs[i:i+batch_size])
            y_batches.append(self._training_outputs[i:i+batch_size])
        num_batches = len(x_batches)

        start = time.time()
        # TODO fix this below to be the matrix again
        #  self._weights_variance = torch.Tensor()

        iter_count = 0
        has_converged = False
        while iter_count < iterations and has_converged == False:
            iter_count += 1
            batch_no = iter_count % num_batches

            grads = [self._gradient_params_log_likelihood(input=training_input,
                                                          output=training_output)
                     for training_input, training_output
                     in zip(x_batches[batch_no], y_batches[batch_no])]
            weights_grad = sum(grad[0] for grad in grads) * num_batches
            noise_grad = sum(grad[1] for grad in grads) * num_batches
            if self._weights_prior is not None:
                # TODO scale this by bz over total data
                weights_grad += torch.from_numpy(
                                    self._weights_prior.gradient_output(
                                        outputs=np.array([self._weights.data.numpy()]),
                                        inputs=[])
                                )


            # TODO add in weight decay
            self._weights.data += self._learning_rate * weights_grad * 1 / torch.norm(weights_grad)
            self._noise_variance.data += self._learning_rate * noise_grad * 1 / torch.norm(noise_grad) # type: ignore


            train_outputs = torch.mm(self._training_inputs, self._weights.unsqueeze(0).transpose(0, 1)).squeeze()
            train_y = self._training_outputs.data.numpy().flatten()
            rmse = mean_squared_error(train_outputs.data.numpy(), train_y)


            if rmse < self._best_rmse:
                self._converged_count = 0
                self._best_rmse = rmse
            else:
                self._converged_count += 1
            if self._converged_count > 1000:
                # TODO optionally fix this to be better frank hutter convergence assessment tools
                #  print("BEST RMSE WAS ", self._best_rmse)
                self._has_finished = True
                break
        #print(self._iterations_done)
        self._iterations_done += iter_count

    def get_params(self) -> Params:
        return Params(
                    weights=ndarray(self._weights[:-1].data.numpy()),
                    offset=float(self._weights[-1].data.numpy()),
                    noise_variance=float(self._noise_variance.data.numpy()[0]),  # type: ignore
                    weights_variance=ndarray(self._weights_variance.data.numpy()) # type: ignore
               )

    def set_params(self, *, params: Params) -> None:
        full_weights = np.append(params['weights'], params['offset'])
        self._weights = to_variable(full_weights, requires_grad=True)
        self._weights.retain_grad()

        self._weights_variance = to_variable(params['weights_variance'], requires_grad=True)
        self._noise_variance = to_variable(params['noise_variance'], requires_grad=True)

    def log_likelihoods(self, *, outputs: Outputs, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[ndarray]: # type: ignore
        # this uses the MAP estimate of current weights to evaluate a likelihood
        # under the model Y ~ Normal(X*w, I)
        #

        """
        input : D-length numpy ndarray
        output : float
        Calculates
        log(normal_density(self.weights * self.input - output, identity * self.noise_variance))
        for a single input/output pair.
        """
        result = np.array([self._log_likelihood(output=to_variable(output),
                                              input=to_variable(input)).data.numpy()
                            for input, output in zip(inputs, outputs)])
        return CallResult(result)

    def log_likelihood(self, *, outputs: Outputs, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[float]:

        inputs = self._offset_input(inputs=inputs)
        result = self.log_likelihoods(outputs=outputs, inputs=inputs, timeout=timeout, iterations=iterations)

        return CallResult(sum(result.value), has_finished=result.has_finished, iterations_done=result.iterations_done)

    def _log_likelihood(self, output: torch.autograd.Variable,
                        input: torch.autograd.Variable) -> \
            torch.autograd.Variable:
        """
        All inputs are torch tensors (or variables if grad desired).
        input : D-length torch to_variable
        output : float
        """
        expected_output = torch.dot(self._weights, input).unsqueeze(0)
        covariance = to_variable(self._noise_variance).view(1, 1)
        #
        return log_mvn_likelihood(expected_output, covariance, output)

    def _gradient_params_log_likelihood(self,
                                        *,
                                        output: torch.autograd.Variable,
                                        input: torch.autograd.Variable) -> Tuple[torch.autograd.Variable, torch.autograd.Variable]:
        """
        Output is ( D-length torch variable, 1-length torch variable )
        """

        self._weights = refresh_node(self._weights)
        self._noise_variance = refresh_node(self._noise_variance)
        log_likelihood = self._log_likelihood(output=output, input=input)
        log_likelihood.backward()
        return self._weights.grad.data, self._noise_variance.grad.data # type: ignore

    def _gradient_output_log_likelihood(self, *, output: ndarray, input: torch.autograd.Variable) -> torch.autograd.Variable:
        """
        output is D-length torch variable
        """

        output_var = to_variable(output)
        log_likelihood = self._log_likelihood(output=output_var, input=input)
        log_likelihood.backward()
        return output_var.grad

    def gradient_output(self, *, outputs: Outputs, inputs: Inputs) -> Gradients[Outputs]: # type: ignore
        # TODO fix comment
        """
        Calculates grad_output log normal_density(self.weights * self.input - output, identity * self.noise_variance)
        for a single input/output pair.
        """
        inputs = self._offset_input(inputs=inputs)

        outputs_vars = [to_variable(output, requires_grad=True) for output in outputs]
        inputs_vars = [to_variable(input) for input in inputs]
        grad = sum(self._gradient_output_log_likelihood(output=output,
                                                        input=input)
                   for (input, output) in zip(inputs_vars, outputs_vars))


        return grad.data.numpy() # type: ignore

    def gradient_params(self, *, outputs: Outputs, inputs: Inputs) -> Gradients[Params]:  # type: ignore
        # TODO fix comment
        # TODO write test for this
        """
        Calculates grad_weights fit_term_temperature *
        log normal_density(self.weights * self.input - output, identity * self.noise_variance)
        for a single input/output pair.
        """

        outputs_vars = [to_variable(output, requires_grad=True) for output in outputs]
        inputs_vars = [to_variable(input) for input in inputs]

        grads = [self._gradient_params_log_likelihood(output=output, input=input)
                 for (input, output) in zip(inputs_vars, outputs_vars)]
        grad_weights = sum(grad[0] for grad in grads[:-1])
        grad_offset = sum(grad for grad in grads[-1])
        grad_noise_variance = sum(grad[1] for grad in grads)


        # TODO weights var
        return Params(weights=grad_weights, offset=grad_weights, noise_variance=grad_noise_variance, weights_variance=0.0) # type: ignore

    def set_fit_term_temperature(self, *, temperature: float = 0) -> None:
        self._fit_term_temperature = temperature

    def _sample_once(self, *, inputs: Inputs) -> Outputs:
        """
        input : NxD numpy ndarray
        outputs : N-length numpy ndarray
        """
        if self._weights is None or self._noise_variance is None or self._weights_variance is None:
            raise ValueError("Params not set.")

        inputs = self._offset_input(inputs=inputs)

        # first sample weights
        # TODO look at bleis paper for weight sampling in gradient opt
        # TODO should have an if here already for which condition triggered gradient N < P or large N

        weights = np.random.multivariate_normal(
                        self._weights.data.numpy(),
                        self._weights_variance.data.numpy() # type: ignore
                    )

        # sample outputs
        output_means = [np.dot(weights, input) for input in inputs]
        outputs = np.random.normal(output_means, self._noise_variance.data[0]) # type: ignore

        return outputs

    def sample(self, *, inputs: Inputs, num_samples: int = 1, timeout: float = None, iterations: int = None) -> CallResult[Sequence[Outputs]]:
        """
        input : num_inputs x D numpy ndarray
        outputs : num_predictions x num_inputs numpy ndarray
        """

        return CallResult([self._sample_once(inputs=inputs) for _ in range(num_samples)])

    def _offset_input(self, *, inputs: Inputs) -> Inputs:
        if inputs.shape[1] == self._weights.shape[0]:
            return inputs
        else:
            return np.insert(inputs, inputs.shape[1], 1, axis=1)

    def get_call_metadata(self) -> CallResult:
        return CallResult(None, has_finished=self._has_finished, iterations_done=self._iterations_done)
