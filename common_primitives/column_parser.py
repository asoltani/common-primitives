import hashlib
import os
import typing

import dateutil.parser

from d3m import container, utils as d3m_utils
from d3m.metadata import base as metadata_base, hyperparams
from d3m.primitive_interfaces import base, transformer

import common_primitives
from common_primitives import utils

__all__ = ('ColumnParserPrimitive',)

Inputs = container.DataFrame
Outputs = container.DataFrame


class Hyperparams(hyperparams.Hyperparams):
    parse_semantic_types = hyperparams.Set(
        elements=hyperparams.Enumeration(
            values=['http://schema.org/Boolean', 'https://metadata.datadrivendiscovery.org/types/CategoricalData', 'http://schema.org/Integer', 'http://schema.org/Float', 'http://schema.org/Time'],
            # Default is ignored.
            default='http://schema.org/Boolean',
        ),
        default=('http://schema.org/Boolean', 'https://metadata.datadrivendiscovery.org/types/CategoricalData', 'http://schema.org/Integer', 'http://schema.org/Float', 'http://schema.org/Time'),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of semantic types to parse. One can provide a subset of supported semantic types to limit what the primitive parses.",
    )
    use_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to force primitive to operate on. If any specified column cannot be parsed, it is skipped.",
    )
    exclude_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to not operate on. Applicable only if \"use_columns\" is not provided.",
    )
    return_result = hyperparams.Enumeration(
        values=['append', 'replace', 'new'],
        default='replace',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Should parsed columns be appended, should they replace original columns, or should only parsed columns be returned?",
    )
    add_index_column = hyperparams.UniformBool(
        default=True,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Also include a primary index column if input data has it. Applicable only if \"return_result\" is set to \"new\".",
    )


class ColumnParserPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which parses strings into their parsed values.

    It goes over all columns (by default, controlled by ``use_columns``, ``exclude_columns``)
    and checks those with structural type ``str`` if they have a semantic type suggesting
    that they are a boolean value, categorical, integer, float, or time (by default,
    controlled by ``parse_semantic_types``). Categorical values are converted with
    hash encoding.

    What is returned is controlled by ``return_result`` and ``add_index_column``.
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': 'd510cb7a-1782-4f51-b44c-58f0236e47c7',
            'version': '0.3.0',
            'name': "Parses strings into their types",
            'python_path': 'd3m.primitives.data.ColumnParser',
            'source': {
               'name': common_primitives.__author__,
            },
            'installation': [{
               'type': metadata_base.PrimitiveInstallationType.PIP,
               'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                   git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
               ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.DATA_CONVERSION,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION,
        },
    )

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        columns_to_produce, output_columns, output_metadata_lists = self._produce_columns(inputs.metadata, inputs, self.hyperparams)

        outputs = utils.combine_columns(self.hyperparams['return_result'], self.hyperparams['add_index_column'], inputs, columns_to_produce, output_columns, output_metadata_lists, self)

        return base.CallResult(outputs)

    @classmethod
    def _can_produce_column(cls, inputs_metadata: metadata_base.DataMetadata, column_index: int, hyperparams: Hyperparams) -> bool:
        column_metadata = inputs_metadata.query((metadata_base.ALL_ELEMENTS, column_index))

        # We produce only on columns which have not yet been parsed (are strings).
        if column_metadata['structural_type'] != str:
            return False

        semantic_types = column_metadata.get('semantic_types', [])

        for semantic_type in hyperparams['parse_semantic_types']:
            if semantic_type not in semantic_types:
                continue

            # Skip parsing if a column is categorical, but also a target column.
            if semantic_type == 'https://metadata.datadrivendiscovery.org/types/CategoricalData' and 'https://metadata.datadrivendiscovery.org/types/Target' in semantic_types:
                continue

            return True

        return False

    @classmethod
    def _produce_columns(cls, inputs_metadata: metadata_base.DataMetadata, inputs_data: typing.Optional[Outputs], hyperparams: Hyperparams) \
            -> typing.Tuple[typing.List[int], typing.List[Outputs], typing.Sequence[typing.List[typing.Tuple[metadata_base.Selector, typing.Dict]]]]:
        columns_to_produce = cls._get_columns(inputs_metadata, hyperparams)

        # We check against this list again because if "use_columns" is used,
        # we still want to respect "parse_semantic_types'.
        parse_semantic_types = hyperparams['parse_semantic_types']

        if inputs_data is None:
            output_columns = None
        else:
            output_columns = []
        output_metadata_lists = []

        # We make a copy of "columns_to_produce" so that we can modify it while iterating.
        for column_index in list(columns_to_produce):
            column_metadata = inputs_metadata.query((metadata_base.ALL_ELEMENTS, column_index))
            semantic_types = column_metadata.get('semantic_types', [])
            if column_metadata['structural_type'] == str:
                if 'http://schema.org/Boolean' in parse_semantic_types and 'http://schema.org/Boolean' in semantic_types:
                    if inputs_data is not None:
                        output_columns.append(cls._parse_boolean_data(inputs_data, column_index))

                    output_metadata_lists.append(cls._parse_boolean_metadata(inputs_metadata, column_index))

                # Here we do parse a target column, so one can force that using "use_columns".
                elif 'https://metadata.datadrivendiscovery.org/types/CategoricalData' in parse_semantic_types and 'https://metadata.datadrivendiscovery.org/types/CategoricalData' in semantic_types:
                    if inputs_data is not None:
                        output_columns.append(cls._parse_categorical_data(inputs_data, column_index))

                    output_metadata_lists.append(cls._parse_categorical_metadata(inputs_metadata, column_index))

                elif 'http://schema.org/Integer' in semantic_types:
                    # For primary key we know all values have to exist so we can assume they can always be represented as integers.
                    if 'https://metadata.datadrivendiscovery.org/types/PrimaryKey' in semantic_types:
                        integer_required = True
                    else:
                        integer_required = False

                    if inputs_data is not None:
                        column_data, column_metadata = cls._parse_integer(inputs_data, inputs_metadata, column_index, integer_required)
                        output_columns.append(column_data)
                        output_metadata_lists.append(column_metadata)
                    else:
                        # Without data we assume we can parse everything into integers. This might not be true and
                        # we might end up parsing into floats if we have to represent missing (or invalid) values.
                        output_metadata_lists.append([
                            ((), {'structural_type': int}),
                        ])

                elif 'http://schema.org/Float' in semantic_types:
                    if inputs_data is not None:
                        output_columns.append(cls._parse_float_data(inputs_data, column_index))

                    output_metadata_lists.append(cls._parse_float_metadata(inputs_metadata, column_index))

                elif 'http://schema.org/Time' in semantic_types:
                    if inputs_data is not None:
                        output_columns.append(cls._parse_time_data(inputs_data, column_index))

                    output_metadata_lists.append(cls._parse_time_metadata(inputs_metadata, column_index))

                else:
                    assert False, column_index

        # We are OK if no columns ended up being parsed.
        # "utils.combine_columns" will throw an error if it cannot work with this.

        assert output_columns is None or len(output_columns) == len(columns_to_produce)
        assert len(output_metadata_lists) == len(columns_to_produce)

        return columns_to_produce, output_columns, output_metadata_lists

    @classmethod
    def _get_columns(cls, inputs_metadata: metadata_base.DataMetadata, hyperparams: Hyperparams) -> typing.List[int]:
        def can_produce_column(column_index: int) -> bool:
            return cls._can_produce_column(inputs_metadata, column_index, hyperparams)

        columns_to_produce, columns_not_to_produce = utils.get_columns_to_produce(inputs_metadata, hyperparams, can_produce_column)

        # We are OK if no columns ended up being parsed.
        # "utils.combine_columns" will throw an error if it cannot work with this.

        if hyperparams['use_columns'] and columns_not_to_produce:
            cls.logger.warning("Not all specified columns can parsed. Skipping columns: %(columns)s", {
                'columns': columns_not_to_produce,
            })

        return columns_to_produce

    @classmethod
    def _parse_boolean_data(cls, inputs_data: Outputs, column_index: metadata_base.SimpleSelectorSegment) -> Outputs:
        return cls._parse_categorical_data(inputs_data, column_index)

    @classmethod
    def _parse_boolean_metadata(cls, inputs_metadata: metadata_base.DataMetadata, column_index: metadata_base.SimpleSelectorSegment) -> typing.List[typing.Tuple[metadata_base.Selector, typing.Dict]]:
        return cls._parse_categorical_metadata(inputs_metadata, column_index)

    @classmethod
    def _parse_categorical_data(cls, inputs_data: Outputs, column_index: metadata_base.SimpleSelectorSegment) -> Outputs:
        values_map: typing.Dict[str, int] = {}
        for value in inputs_data.iloc[:, column_index]:
            value = value.strip()
            if value not in values_map:
                value_hash = hashlib.sha256(value.encode('utf8'))
                values_map[value] = int.from_bytes(value_hash.digest()[0:8], byteorder='little') ^ int.from_bytes(value_hash.digest()[8:16], byteorder='little') ^ \
                    int.from_bytes(value_hash.digest()[16:24], byteorder='little') ^ int.from_bytes(value_hash.digest()[24:32], byteorder='little')

        return container.DataFrame([values_map[value.strip()] for value in inputs_data.iloc[:, column_index]], generate_metadata=False)

    @classmethod
    def _parse_categorical_metadata(cls, inputs_metadata: metadata_base.DataMetadata,
                                    column_index: metadata_base.SimpleSelectorSegment) -> typing.List[typing.Tuple[metadata_base.Selector, typing.Dict]]:
        return [((), {'structural_type': int})]

    @classmethod
    def _str_to_int(cls, value: str) -> typing.Union[float, int]:
        try:
            return int(value.strip())
        except ValueError:
            try:
                # Maybe it is an int represented as a float. Let's try this. This can get rid of non-integer
                # part of the value, but the integer was requested through a semantic type, so this is probably OK.
                return int(float(value.strip()))
            except ValueError:
                # No luck, use NaN to represent a missing value.
                return float('nan')

    @classmethod
    def _parse_integer(cls, inputs_data: Outputs, inputs_metadata: metadata_base.DataMetadata, column_index: metadata_base.SimpleSelectorSegment,
                       integer_required: bool) -> typing.Tuple[Outputs, typing.List[typing.Tuple[metadata_base.Selector, typing.Dict]]]:
        column_data = container.DataFrame([cls._str_to_int(value) for value in inputs_data.iloc[:, column_index]], generate_metadata=False)

        if column_data.dtypes.iloc[0].kind == 'f':
            structural_type: type = float
        elif column_data.dtypes.iloc[0].kind == 'i':
            structural_type = int
        else:
            assert False, column_data.dtypes.iloc[0]

        if structural_type is float and integer_required:
            raise ValueError("Not all values in a column can be parsed into integers, but only integers were expected.")

        column_metadata = typing.cast(typing.List[typing.Tuple[metadata_base.Selector, typing.Dict]], [((), {'structural_type': structural_type})])

        return column_data, column_metadata

    @classmethod
    def _str_to_float(cls, value: str) -> float:
        try:
            return float(value.strip())
        except ValueError:
            return float('nan')

    @classmethod
    def _parse_float_data(cls, inputs_data: Outputs, column_index: metadata_base.SimpleSelectorSegment) -> Outputs:
        return container.DataFrame([cls._str_to_float(value) for value in inputs_data.iloc[:, column_index]], generate_metadata=False)

    @classmethod
    def _parse_float_metadata(cls, inputs_metadata: metadata_base.DataMetadata, column_index: metadata_base.SimpleSelectorSegment) -> typing.List[typing.Tuple[metadata_base.Selector, typing.Dict]]:
        return [((), {'structural_type': float})]

    @classmethod
    def _time_to_foat(cls, value: str) -> float:
        try:
            return dateutil.parser.parse(value).timestamp()
        except (ValueError, OverflowError):
            return float('nan')

    @classmethod
    def _parse_time_data(cls, inputs_data: Outputs, column_index: metadata_base.SimpleSelectorSegment) -> Outputs:
        return container.DataFrame([cls._time_to_foat(value) for value in inputs_data.iloc[:, column_index]], generate_metadata=False)

    @classmethod
    def _parse_time_metadata(cls, inputs_metadata: metadata_base.DataMetadata, column_index: metadata_base.SimpleSelectorSegment) -> typing.List[typing.Tuple[metadata_base.Selector, typing.Dict]]:
        return [((), {'structural_type': float})]

    @classmethod
    def can_accept(cls, *, method_name: str, arguments: typing.Dict[str, typing.Union[metadata_base.Metadata, type]], hyperparams: Hyperparams) -> typing.Optional[metadata_base.DataMetadata]:
        output_metadata = super().can_accept(method_name=method_name, arguments=arguments, hyperparams=hyperparams)

        # If structural types didn't match, don't bother.
        if output_metadata is None:
            return None

        if 'inputs' not in arguments:
            return output_metadata

        inputs_metadata = typing.cast(metadata_base.DataMetadata, arguments['inputs'])

        columns_to_produce, _, output_metadata_lists = cls._produce_columns(inputs_metadata, None, hyperparams)

        # We are stricter here than "produce" because we are not really useful.
        if not columns_to_produce:
            return None

        return utils.combine_columns_metadata(hyperparams['return_result'], hyperparams['add_index_column'], inputs_metadata, columns_to_produce, output_metadata_lists, cls)
