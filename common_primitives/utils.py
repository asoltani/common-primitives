import copy
import math
import typing
from typing import *

import frozendict  # type: ignore
import torch  # type: ignore
import pandas  # type: ignore
from torch.autograd import Variable  # type: ignore
import numpy as np  # type: ignore

from d3m import container, exceptions
from d3m.metadata import base as metadata_base, hyperparams as metadata_hyperparams


def add_dicts(dict1: typing.Dict, dict2: typing.Dict) -> typing.Dict:
    summation = {}
    for key in dict1:
        summation[key] = dict1[key] + dict2[key]
    return summation


def sum_dicts(dictArray: typing.Sequence[typing.Dict]) -> typing.Dict:
    assert len(dictArray) > 0
    summation = dictArray[0]
    for dictionary in dictArray:
        summation = add_dicts(summation, dictionary)
    return summation


def to_variable(value: Any, requires_grad: bool = False) -> Variable:
    """
    Converts an input to torch Variable object
    input
    -----
    value - Type: scalar, Variable object, torch.Tensor, numpy ndarray
    requires_grad  - Type: bool . If true then we require the gradient of that object

    output
    ------
    torch.autograd.variable.Variable object
    """

    if isinstance(value, Variable):
        return value
    elif torch.is_tensor(value):
        return Variable(value.float(), requires_grad=requires_grad)
    elif isinstance(value, np.ndarray) or isinstance(value, container.ndarray):
        return Variable(torch.from_numpy(value.astype(float)).float(), requires_grad=requires_grad)
    elif value is None:
        return None
    else:
        return Variable(torch.Tensor([float(value)]), requires_grad=requires_grad)


def to_tensor(value: Any) -> torch.FloatTensor:
    """
    Converts an input to a torch FloatTensor
    """
    if isinstance(value, np.ndarray):
        return torch.from_numpy(value).float()
    else:
        raise ValueError('Unsupported type: {}'.format(type(value)))


def refresh_node(node: Variable) -> Variable:
    return torch.autograd.Variable(node.data, True)


def log_mvn_likelihood(mean: torch.FloatTensor, covariance: torch.FloatTensor, observation: torch.FloatTensor) -> torch.FloatTensor:
    """
    all torch primitives
    all non-diagonal elements of covariance matrix are assumed to be zero
    """
    k = mean.shape[0]
    variances = covariance.diag()
    log_likelihood = 0
    for i in range(k):
        log_likelihood += - 0.5 * torch.log(variances[i]) \
                          - 0.5 * k * math.log(2 * math.pi) \
                          - 0.5 * ((observation[i] - mean[i])**2 / variances[i])
    return log_likelihood


def covariance(data: torch.FloatTensor) -> torch.FloatTensor:
    """
    input: NxD torch array
    output: DxD torch array

    calculates covariance matrix of input
    """

    N, D = data.size()
    cov = torch.zeros([D, D]).type(torch.DoubleTensor)
    for contribution in (torch.matmul(row.view(D, 1),
                         row.view(1, D))/N for row in data):
        cov += contribution
    return cov


def remove_mean(data: torch.FloatTensor) -> typing.Tuple[torch.FloatTensor, torch.FloatTensor]:
    """
    input: NxD torch array
    output: D-length mean vector, NxD torch array

    takes a torch tensor, calculates the mean of each
    column and subtracts it

    returns (mean, zero_mean_data)
    """

    N, D = data.size()
    mean = torch.zeros([D]).type(torch.DoubleTensor)
    for row in data:
        mean += row.view(D)/N
    zero_mean_data = data - mean.view(1, D).expand(N, D)
    return mean, zero_mean_data


def denumpify(unknown_object: typing.Any) -> typing.Any:
    """
    changes 'numpy.int's and 'numpy.float's etc to standard Python equivalents
    no effect on other data types
    """
    try:
        return unknown_object.item()
    except AttributeError:
        return unknown_object


M = typing.TypeVar('M', bound=metadata_base.Metadata)


# A copy of "Metadata._query" which starts ignoring "ALL_ELEMENTS" only at a certain depth.
# TODO: Make this part of metadata API.
def _query(selector: metadata_base.Selector, metadata_entry: typing.Optional[metadata_base.MetadataEntry], ignore_all_elements: int = None) -> frozendict.FrozenOrderedDict:
    if metadata_entry is None:
        return frozendict.FrozenOrderedDict()
    if len(selector) == 0:
        return metadata_entry.metadata

    segment, selector_rest = selector[0], selector[1:]

    if ignore_all_elements is not None:
        new_ignore_all_elements = ignore_all_elements - 1
    else:
        new_ignore_all_elements = None

    all_elements_metadata = _query(selector_rest, metadata_entry.all_elements, new_ignore_all_elements)
    if segment is metadata_base.ALL_ELEMENTS:
        metadata = all_elements_metadata
    elif segment in metadata_entry.elements:
        segment = typing.cast(metadata_base.SimpleSelectorSegment, segment)
        metadata = _query(selector_rest, metadata_entry.elements[segment], new_ignore_all_elements)
        if ignore_all_elements is None or ignore_all_elements > 0:
            metadata = metadata_base.Metadata()._merge_metadata(all_elements_metadata, metadata)
    elif ignore_all_elements is not None and ignore_all_elements <= 0:
        metadata = frozendict.FrozenOrderedDict()
    else:
        metadata = all_elements_metadata

    return metadata


# TODO: Make this part of metadata API.
def copy_elements_metadata(source_metadata: metadata_base.Metadata, selector_prefix: metadata_base.Selector,
                           selector: metadata_base.Selector, target_metadata: M, *, source: typing.Any = None) -> M:
    """
    Recursively copies metadata from source to target, starting at the elements of the supplied "selector_prefix" + "selector".
    It does not copy metadata at the "selector_prefix" + "selector" itself.
    """

    # "ALL_ELEMENTS" is always first, if it exists, which works in our favor here.
    elements = source_metadata.get_elements(list(selector_prefix) + list(selector))

    for element in elements:
        new_selector = list(selector) + [element]
        metadata = _query(list(selector_prefix) + new_selector, source_metadata._current_metadata, len(selector_prefix))
        target_metadata = target_metadata.update(new_selector, metadata, source=source)
        target_metadata = copy_elements_metadata(source_metadata, selector_prefix, new_selector, target_metadata, source=source)

    return target_metadata


# TODO: Make this part of metadata API.
def metadata_select_columns(source_metadata: metadata_base.DataMetadata, columns: typing.Sequence[metadata_base.SimpleSelectorSegment], *,
                            source: typing.Any = None) -> metadata_base.DataMetadata:
    assert len(set(columns)) == len(columns)

    assert any(not isinstance(column_index, int) for column_index in columns) or 0 <= min(typing.cast(typing.Sequence[int], columns))

    # Do nothing if selecting all columns.
    if source_metadata.query((metadata_base.ALL_ELEMENTS,)).get('dimension', {}).get('length', None) == len(columns):
        return source_metadata

    # This makes a copy.
    output_metadata = source_metadata.update(
        (metadata_base.ALL_ELEMENTS,),
        {
            'dimension': {
                'length': len(columns),
            },
        },
        source=source,
    )

    # TODO: Do this better. This change is missing an entry in metadata log.
    elements = output_metadata._current_metadata.all_elements.elements
    output_metadata._current_metadata.all_elements.elements = {}
    for i, column_index in enumerate(sorted(columns)):
        if column_index in elements:
            # If "column_index" is really numeric, we re-enumerate it.
            if isinstance(column_index, int):
                output_metadata._current_metadata.all_elements.elements[i] = elements[column_index]
            else:
                output_metadata._current_metadata.all_elements.elements[column_index] = elements[column_index]

    return output_metadata


# TODO: Make this part of metadata API.
def metadata_list_columns_with_semantic_types(metadata: metadata_base.DataMetadata, semantic_types: typing.Sequence[str]) -> typing.Sequence[int]:
    """
    This is similar to "get_columns_with_semantic_type", but it returns all columns indices which should
    exist in a dimension and not just those which exist in metadata elements (so it returns also columns
    which do not have metadata, and it does not return metadata for "ALL_ELEMENTS").
    """

    columns = []

    for column_index in range(metadata.query((metadata_base.ALL_ELEMENTS,)).get('dimension', {}).get('length', 0)):
        if any(metadata.has_semantic_type((metadata_base.ALL_ELEMENTS, column_index), semantic_type) for semantic_type in semantic_types):
            columns.append(column_index)

    return columns


def get_columns_to_produce(metadata: metadata_base.DataMetadata, hyperparams: metadata_hyperparams.Hyperparams, can_produce_column: typing.Callable) -> typing.Tuple[typing.List[int], typing.List[int]]:
    all_columns = list(hyperparams['use_columns'])

    if not all_columns:
        all_columns = list(range(metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']))

        all_columns = [column_index for column_index in all_columns if column_index not in hyperparams['exclude_columns']]

    columns_to_produce = []
    columns_not_to_produce = []
    for column_index in all_columns:
        if can_produce_column(column_index):
            columns_to_produce.append(column_index)
        else:
            columns_not_to_produce.append(column_index)

    return columns_to_produce, columns_not_to_produce


# TODO: What happens to the metadata for rows? It should be copied over as well for copied columns.
def append_metadata(left_metadata: metadata_base.DataMetadata, right_metadata: metadata_base.DataMetadata, source: typing.Any) -> metadata_base.DataMetadata:
    """
    Top-level metadata of ``right_metadata`` is ignored.
    """

    left_columns = left_metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']
    outputs_metadata = left_metadata

    for column_index in range(right_metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']):
        # Assumption is that left and right metadata are from different origins,
        # so we have to query here and include also "ALL_ELEMENTS".
        metadata = right_metadata.query([metadata_base.ALL_ELEMENTS, column_index])
        outputs_metadata = outputs_metadata.update([metadata_base.ALL_ELEMENTS, left_columns + column_index], metadata, source=source)
        outputs_metadata = copy_elements_metadata(right_metadata, [metadata_base.ALL_ELEMENTS, column_index], [metadata_base.ALL_ELEMENTS, left_columns + column_index], outputs_metadata, source=source)
    index_columns = outputs_metadata.get_columns_with_semantic_type('https://metadata.datadrivendiscovery.org/types/PrimaryKey')
    index_columns = typing.cast(typing.List[int], list(index_columns))
    if len(index_columns) > 1:
        # Remove all except first primary index semantic types, and convert it to unique key.
        for column_index in sorted(index_columns)[1:]:
            outputs_metadata = outputs_metadata.remove_semantic_type([metadata_base.ALL_ELEMENTS, column_index], 'https://metadata.datadrivendiscovery.org/types/PrimaryKey', source=source)
            outputs_metadata = outputs_metadata.add_semantic_type([metadata_base.ALL_ELEMENTS, column_index], 'https://metadata.datadrivendiscovery.org/types/UniqueKey', source=source)

    return outputs_metadata


# TODO: What happens to the metadata for rows? It should be copied over as well for copied columns.
def combine_columns_metadata(return_result: str, add_index_column: bool, inputs_metadata: metadata_base.DataMetadata, column_indices: typing.List[int],
                    metadata_list: typing.Sequence[typing.List[typing.Tuple[metadata_base.Selector, typing.Dict]]],
                    source: typing.Any) -> metadata_base.DataMetadata:
    """
    Method which appends existing columns, replaces them, or creates new result from them.

    It operates only on metadata.
    """

    assert len(metadata_list) == len(column_indices)

    if return_result == 'append':
        columns_number = inputs_metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']
        outputs_metadata = inputs_metadata
        # We first copy metadata from source columns over.
        for i, column_index in enumerate(column_indices):
            # "ALL_ELEMENTS" already apply to all columns, so there is no point in copying metadata with that metadata combined.
            metadata = inputs_metadata.query([metadata_base.ALL_ELEMENTS, column_index], ignore_all_elements=True)
            outputs_metadata = outputs_metadata.update([metadata_base.ALL_ELEMENTS, columns_number + i], metadata, source=source)
            outputs_metadata = copy_elements_metadata(inputs_metadata, [metadata_base.ALL_ELEMENTS, column_index], [metadata_base.ALL_ELEMENTS, columns_number + i], outputs_metadata, source=source)
        index_columns = outputs_metadata.get_columns_with_semantic_type('https://metadata.datadrivendiscovery.org/types/PrimaryKey')
        index_columns = typing.cast(typing.List[int], list(index_columns))
        if len(index_columns) > 1:
            # Remove all except first primary index semantic types, and convert it to unique key.
            for column_index in sorted(index_columns)[1:]:
                outputs_metadata = outputs_metadata.remove_semantic_type([metadata_base.ALL_ELEMENTS, column_index], 'https://metadata.datadrivendiscovery.org/types/PrimaryKey', source=source)
                outputs_metadata = outputs_metadata.add_semantic_type([metadata_base.ALL_ELEMENTS, column_index], 'https://metadata.datadrivendiscovery.org/types/UniqueKey', source=source)
        for i, column_metadata in enumerate(metadata_list):
            for selector, metadata in column_metadata:
                outputs_metadata = outputs_metadata.update([metadata_base.ALL_ELEMENTS, columns_number + i] + list(selector), metadata, source=source)

    elif return_result == 'replace':
        outputs_metadata = inputs_metadata
        for column_index, column_metadata in zip(column_indices, metadata_list):
            for selector, metadata in column_metadata:
                outputs_metadata = outputs_metadata.update([metadata_base.ALL_ELEMENTS, column_index] + list(selector), metadata, source=source)  # type: ignore

    elif return_result == 'new':
        add_columns = 0
        if add_index_column:
            index_columns = inputs_metadata.get_columns_with_semantic_type('https://metadata.datadrivendiscovery.org/types/PrimaryKey')
            index_columns = typing.cast(typing.List[int], list(index_columns))
            # There should be really only one. We traverse in reverse order because we are prefixing one by one.
            for column_index in reversed(index_columns):
                if column_index not in column_indices:
                    column_indices = [column_index] + column_indices
                    add_columns += 1
        outputs_metadata = metadata_select_columns(inputs_metadata, column_indices, source=source)
        for i, column_metadata in enumerate(metadata_list):
            for selector, metadata in column_metadata:
                outputs_metadata = outputs_metadata.update([metadata_base.ALL_ELEMENTS, i + add_columns] + list(selector), metadata, source=source)  # type: ignore

    else:
        raise exceptions.InvalidArgumentValueError("\"return_result\" has an invalid value: {return_result}".format(return_result=return_result))

    return outputs_metadata


# TODO: What happens to the metadata for rows? It should be copied over as well for copied columns.
def combine_columns(return_result: str, add_index_column: bool, inputs: container.DataFrame, column_indices: typing.List[int],
                    columns: typing.List[container.DataFrame], metadata_list: typing.Sequence[typing.List[typing.Tuple[metadata_base.Selector, typing.Dict]]],
                    source: typing.Any) -> container.DataFrame:
    """
    Method which appends existing columns, replaces them, or creates new result from them.

    It operates on both data and metadata. Metadata in "columns" is ignored. The assumption is that "columns" are
    coming from "inputs" DataFrame, but that there were maybe some modifications to them.
    """

    # Currently every DataFrame in "columns" have to contain only one column.
    assert all(column.shape[1] == 1 for column in columns)

    assert len(columns) == len(column_indices)
    assert len(metadata_list) == len(column_indices)

    if return_result == 'append':
        if column_indices:
            columns_number = inputs.shape[1]
            outputs = pandas.concat([inputs] + list(columns), axis=1)
            outputs.metadata = inputs.metadata.set_for_value(outputs, source=source, generate_metadata=False)
            # We first copy metadata from source columns over.
            for i, column_index in enumerate(column_indices):
                metadata = inputs.metadata.query([metadata_base.ALL_ELEMENTS, column_index], ignore_all_elements=True)
                outputs.metadata = outputs.metadata.update([metadata_base.ALL_ELEMENTS, columns_number + i], metadata, source=source)
                outputs.metadata = copy_elements_metadata(inputs.metadata, [metadata_base.ALL_ELEMENTS, column_index], [metadata_base.ALL_ELEMENTS, columns_number + i], outputs.metadata, source=source)
            index_columns = outputs.metadata.get_columns_with_semantic_type('https://metadata.datadrivendiscovery.org/types/PrimaryKey')
            index_columns = typing.cast(typing.Sequence[int], list(index_columns))
            if len(index_columns) > 1:
                # Remove all except first primary index semantic types, and convert it to unique key.
                for column_index in sorted(index_columns)[1:]:
                    outputs.metadata = outputs.metadata.remove_semantic_type([metadata_base.ALL_ELEMENTS, column_index], 'https://metadata.datadrivendiscovery.org/types/PrimaryKey', source=source)
                    outputs.metadata = outputs.metadata.add_semantic_type([metadata_base.ALL_ELEMENTS, column_index], 'https://metadata.datadrivendiscovery.org/types/UniqueKey', source=source)
            for i, column_metadata in enumerate(metadata_list):
                for selector, metadata in column_metadata:
                    outputs.metadata = outputs.metadata.update([metadata_base.ALL_ELEMENTS, columns_number + i] + list(selector), metadata, source=source)

    elif return_result == 'replace':
        if column_indices:
            outputs = copy.copy(inputs)
            for column_index, column in zip(column_indices, columns):
                # There is only one column in "column" DataFrame.
                outputs.iloc[:, column_index] = column.iloc[:, 0]
            outputs.metadata = inputs.metadata.set_for_value(outputs, source=source, generate_metadata=False)
            for column_index, column_metadata in zip(column_indices, metadata_list):
                for selector, metadata in column_metadata:
                    outputs.metadata = outputs.metadata.update([metadata_base.ALL_ELEMENTS, column_index] + list(selector), metadata, source=source)  # type: ignore

    elif return_result == 'new':
        if not column_indices:
            raise ValueError("No columns produced.")

        add_columns = 0
        if add_index_column:
            index_columns = inputs.metadata.get_columns_with_semantic_type('https://metadata.datadrivendiscovery.org/types/PrimaryKey')
            index_columns = typing.cast(typing.List[int], list(index_columns))
            # There should be really only one. We traverse in reverse order because we are prefixing one by one.
            for column_index in reversed(index_columns):
                if column_index not in column_indices:
                    column_indices = [column_index] + column_indices
                    columns = [inputs.iloc[:, column_index]] + columns
                    add_columns += 1
        outputs = pandas.concat(columns, axis=1)
        outputs.metadata = metadata_select_columns(inputs.metadata, column_indices, source=source)
        outputs.metadata = outputs.metadata.set_for_value(outputs, source=source, generate_metadata=False)
        for i, column_metadata in enumerate(metadata_list):
            for selector, metadata in column_metadata:
                outputs.metadata = outputs.metadata.update([metadata_base.ALL_ELEMENTS, i + add_columns] + list(selector), metadata, source=source)  # type: ignore

    else:
        raise exceptions.InvalidArgumentValueError("\"return_result\" has an invalid value: {return_result}".format(return_result=return_result))

    return outputs


def remove_column(input: container.DataFrame, column_index: int, source: typing.Any) -> container.DataFrame:
    # We are not using "drop" because we are dropping by the column index (to support columns with same name).
    columns = list(range(input.shape[1]))

    if not columns:
        raise ValueError("No columns to remove.")

    columns.remove(column_index)

    if not columns:
        raise ValueError("Removing a column would have removed the last column.")

    output = input.iloc[:, columns]
    output.metadata = metadata_select_columns(input.metadata, columns, source=source)
    output.metadata = output.metadata.set_for_value(output, generate_metadata=False, source=source)

    return output


def remove_column_metadata(input_metadata: metadata_base.DataMetadata, column_index: int, source: typing.Any) -> metadata_base.DataMetadata:
    columns = list(range(input_metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']))

    if not columns:
        raise ValueError("No columns to remove.")

    columns.remove(column_index)

    if not columns:
        raise ValueError("Removing a column would have removed the last column.")

    return metadata_select_columns(input_metadata, columns, source=source)
