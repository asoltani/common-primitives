import json
import os

import torchvision.datasets as datasets
import torchvision.transforms as transforms

from d3m import utils, container
from d3m.metadata import base as metadata_base


def convert_metadata(metadata):
    return json.loads(json.dumps(metadata, cls=utils.JsonEncoder))


def load_iris_metadata():
    dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'iris_dataset_1', 'datasetDoc.json'))
    dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))
    return dataset


def test_iris_metadata(test_obj, metadata, structural_type):
    test_obj.maxDiff = None

    test_obj.assertEqual(convert_metadata(metadata.query(())), {
        'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
        'structural_type': structural_type,
        'semantic_types': [
            'https://metadata.datadrivendiscovery.org/types/Table',
        ],
        'dimension': {
            'name': 'rows',
            'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
            'length': 150,
        }
    })

    test_obj.assertEqual(convert_metadata(metadata.query((metadata_base.ALL_ELEMENTS,))), {
        'dimension': {
            'name': 'columns',
            'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
            'length': 6,
        }
    })

    test_obj.assertEqual(convert_metadata(metadata.query((metadata_base.ALL_ELEMENTS, 0))), {
        'name': 'd3mIndex',
        'structural_type': 'str',
        'semantic_types': [
            'http://schema.org/Integer',
            'https://metadata.datadrivendiscovery.org/types/PrimaryKey',
        ],
    })

    for i in range(1, 5):
        test_obj.assertEqual(convert_metadata(metadata.query((metadata_base.ALL_ELEMENTS, i))), {
            'name': ['sepalLength', 'sepalWidth', 'petalLength', 'petalWidth'][i - 1],
            'structural_type': 'str',
            'semantic_types': [
                'http://schema.org/Float',
                'https://metadata.datadrivendiscovery.org/types/Attribute',
            ],
        }, i)

    test_obj.assertEqual(convert_metadata(metadata.query((metadata_base.ALL_ELEMENTS, 5))), {
        'name': 'species',
        'structural_type': 'str',
        'semantic_types': [
            'https://metadata.datadrivendiscovery.org/types/CategoricalData',
            'https://metadata.datadrivendiscovery.org/types/SuggestedTarget',
        ],
    })


def get_mnist_train_data(size):
    mnist_root = os.path.dirname(os.path.realpath(__file__)) + '/test_data_mnist'
    download_if_missing = True
    trans = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5,), (1.0,))])
    mnist_train = datasets.MNIST(root=mnist_root, train=True, transform=trans, download=download_if_missing)
    # mnist_test = datasets.MNIST(root=mnist_root, train=False, transform=trans)
    mnist_train_inputs = mnist_train.train_data[:size].numpy()
    mnist_train_outputs = mnist_train.train_labels[:size].numpy()
    return mnist_train_inputs, mnist_train_outputs
