import unittest
import os
import numpy as np

from common_primitives.video_reader import VideoReader, Hyperparams


class VideoReaderTestCase(unittest.TestCase):
    def test_produce(self):
        vr = VideoReader(hyperparams=Hyperparams.defaults())
        test_dir = os.path.dirname(os.path.realpath(__file__)) + "/test_video/"

        data_run = vr.produce(inputs=[test_dir + 'hmdb51_run_1.avi', test_dir + 'hmdb51_run_2.avi']).value

        data_run = np.array(data_run)
        assert((data_run.shape[0] == 2) and (data_run[0].shape[1:] == (240,576,3)) and (data_run[1].shape[1:] == (240,576,3)))

    def test_resize(self):
        vr = VideoReader(hyperparams=Hyperparams(Hyperparams.defaults(), resize_to=(224,224)))
        test_dir = os.path.dirname(os.path.realpath(__file__)) + "/test_video/"

        data_run = vr.produce(inputs=[test_dir + 'hmdb51_run_1.avi', test_dir + 'hmdb51_run_2.avi']).value

        data_run = np.array(data_run)
        assert((data_run.shape[0] == 2) and (data_run[0].shape[1:] == (224,224,3)) and (data_run[1].shape[1:] == (224,224,3)))

if __name__ == '__main__':
    unittest.main()
