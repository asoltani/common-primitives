import unittest
import numpy as np
import pandas as pd
from common_primitives.one_hot_maker import OneHotMaker, Hyperparams


class OneHotTestCase(unittest.TestCase):
    def test_fit(self):
        ht = OneHotMaker(hyperparams=Hyperparams.defaults())

        ht.set_training_data(inputs=pd.DataFrame({"value": [0.0, 1.0, 2.0, 3.0],
                                      "number": [0, 1, 2, 3],
                                      "word": ["one", "two", "three", "four"]}))
        ht.fit()
        inputs = pd.DataFrame({'word': ['one', 'two', 'three'],
                   'number': [1, 2, 3],
                   'value': [1.0, 2.0, 3.0]})
        assert np.array_equal(ht.produce(inputs=inputs).value,
                              np.array([[ 1.,  1.,  1.,  0.,  0.,  0.],
                                        [ 2.,  2.,  0.,  1.,  0.,  0.],
                                        [ 3.,  3.,  0.,  0.,  1.,  0.]]))


if __name__ == '__main__':
    unittest.main()
